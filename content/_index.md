---
title: "Ecología Espacial"
date: 2018-09-17T15:09:24-05:00
draft: true
---

Bienvenidos a la página del curso de Ecología Espacial en la Universidad de los Andes dictado por Carol Ximena Garzón-López. En este lugar encontrarán información y códigos en GRASS GIS y R.
