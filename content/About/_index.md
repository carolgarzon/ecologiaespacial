---
title: "About"

---
### Carol Ximena Garzon-Lopez

I am a Colombian researcher and the director of Verde Elemental, a website in Spanish, dedicated to promoting and disseminating knowledge in ecology and conservation for Latin America.  Currently I am a postdoctoral researcher at Universidad de los Andes (Bogotá, Colombia) teaching the Ecology: Principles and Applications and Spatial ecology courses. During 2014-2016 I worked as a postdoctoral researcher at Fondazione Edmund Mach (Italy), where I provided expertise on the use of remote sensing tools and integrative approaches for species distribution modeling as part of the European Union Biodiversity Observation Network project (EU BON). And during 2016, I was part of the DIARS project where we performed research studies and developed a set of tools to detect, map and monitor invasive plant species through remote sensing. During my time in Italy I became interested in Free and Open Source Software (specifically in GRASS GIS) and since I have taught workshops and develop tools using FOSS software.

My scientific interests include the use of spatial tools for research on conservation in the tropics.  I hold a Master's degree and Ph.D. from Groningen University, Netherlands.  During my Ph.D. I studied the determinants of the spatial distribution of tree species in a tropical forest in Panama, at the Smithsonian Tropical Research Institute, where I have also collaborated in education and public communication.

Contact email: c.x.garzon@gmail.com
