---
title: "Ejercicio - Fragmentación"
date: 2018-08-14T15:18:35-05:00
description: "Análisis del paisaje y de procesos de fragmentación"
featured_image: "/images/programa4.jpg"
draft: false
type: page
---

Los análisis de paisaje se hacen en su mayoría a partir de capas tipo raster que contienen información que permite describir el área en terminos de tipos de coberturas, vecindario y estructura del paisaje. Puede partir de capas tipo poligono que una vez convertidas a raster estarán listas para el análisis.

<br />

## EN GRASS GIS

Vamos a trabajar en la localizacion de __nc_spm_08_grass7__ y en el mapset que cada uno ha creado con su usuario.

Después de iniciar la sesión debemos empezar por instalar dos extensiones: _r.diversity_ y _r.forestfrag_

```
g.extension r.diversity
g.extension r.forestfrag

```
<br />

Ahora iremos al icono de "adicionar capas raster" en la ventana del administrador de capas y en la lista de raster debemos buscar el raster landuse96_28m y agregarlo. Ya estamos listos para empezar con el análisis de paisaje. __No olviden establecer la region computacional a partir de la capa raster landuse96_28m__

- Que tipo de raster es landuse96_28m? Que caracteristica del paisaje describe? A que corresponden los colores? Agreguemos la leyenda para poder ver esta clasificación.
<br />

### Mediciones de riqueza y diversidad.

A partir del comando [_r.neighbors_](https://grass.osgeo.org/grass74/manuals/r.neighbors.html) calcula la categoria de cada pixel basado en los valores de las celdas a su alrededor y guarda las categorias en un mapa raster. Usando el metodo _diversity_ el comando calcula la diversidad de valores alrededor de cada celda.
<br />
{{< figure src="/images/diversity.png" >}}
<br />
```
r.neighbors input=landuse96_28m output=richness method=diversity size=15
```
El comando [_r.diversity_](https://grass.osgeo.org/grass74/manuals/addons/r.diversity.html) estima la diversidad de un rater usando los indices de Shannon, Simpson y Renyi para determinados tamaños de ventana que podemos especificarle al comando:

```
r.diversity input=landuse96_28m prefix=index alpha=0.8 size=9-21 method=simpson,shannon,renyi

```
Ahora vamos a poner el mismo set de colores (viridis) a todas las capas de indices de diversidad para poder compararlas. EN el caso de las capas creadas a partir del indice de Simpson, el set de colores sera grises porque este indice va de 0 a 1.

```
r.colors map=index_shannon_size_21,index_shannon_size_15,index_shannon_size_9 color=viridis
r.colors map=index_renyi_size_21_alpha_0.8,index_renyi_size_15_alpha_0.8,index_renyi_size_15_alpha_0.8 color=viridis
# we use grey1.0 color ramp because simpson is from 0 to 1
r.colors map=index_simpson_size_21,index_simpson_size_15,index_simpson_size_9 color=grey1.0
```

Recuerden que deben agregarlas capas usando el icono de agregar capas en la ventana del administrador de capas

<br />
{{< figure src="/images/diversityA.png" >}}
<br />

- Que diferencias ven entre los indices?
- Como se afectan los indices con el cambio del tamaño de la ventana?

### Fragmentación de bosques

Primero vamos a calcular el indice de fragmentación usando el comando [_r.forestfrag_](https://grass.osgeo.org/grass74/manuals/addons/r.forestfrag.html) que se basa en la metodología propuesta por [Riitters et al. 2000](https://www.ecologyandsociety.org/vol4/iss2/art3/) por medio del cual el paisaje se divide en 6 categorias de acuerdo al tipo de pixel que rodea cada pixel de bosque. Las categorias son: Interior, Undetermined, perforated, edge, transitional y patch.

En este ejercicio vamos a trabajar con la capa raster _landclass96_ asi que antes de empezar la debemos agregar a las capas del administrador de capas y poner la leyenda para ver las caracteristicas de esta capa.

- En que categorias se divide esta capa?

```
# Establecer la region computacional
g.region raster=landclass96

# Otra forma de examinar las categorias de la capa raster es con r.category
r.category map=landclass96

# Uando r.mapcalc vamos a seleccionar las areas de bosque unicamente
r.mapcalc "forest = if(landclass96 == 5, 1, 0)"

```
- Como podemos interpretar la expresion: __if(landclass96 == 5,1,0)__?

Ahora podemos estimar el indice de fragmentación usando el tamaño de la ventana 15:

```
r.forestfrag input=forest output=fragmentation window=15
```
Para ver el resultado lo debemos agregar al administrador de capas y luego podemos agregar la layenda para ver las categorias.

<br />
{{< figure src="/images/forestfrag.png" >}}
<br />

Tambien podremos ver un reporte del analisis de fragmentación, incluyendo el area y los porcentajes de cada categoria.

```
r.report map=fragmentation units=k,p
```
- Si cambian el tamaño de la ventana de analisis que pasa? Intenten con 5, 10 y 20 y miren que sucede.

Para aprender más sobre esta herramienta visiten este [blog](https://pvanb.wordpress.com/2016/03/25/update-of-r-forestfrag-addon/)

#### Distancia al borde del fragmento

Para hacer esta medición seleccionaremos solo las areas de bosque y con el comando [_r.grow.distance_](https://grass.osgeo.org/grass77/manuals/r.grow.distance.html), la orden _-n_ sirve para que el comando calcule la distancia desde el interior del bosque.

```
# Seleccionamos solo la parte de bosques
r.mapcalc "forest = if(landclass96 == 5, 1, null())" --o

r.grow.distance -n input=forest distance=distance

# Para ver las estadisticas de esta capa
r.univar map=distance@landsat                                                   

```

Agreguemos la capa resultante a la ventana del administrador de capas y adicionemos la leyenda.

### Análisis de parches en el paisaje

Para este analisis debemos primero establecer dos archivos _config_:

<br />
{{< figure src="/images/LPA1.png" >}}
<br />

Para el primer archivo:

1. Hacer click en __Create__
2. Llamar el archivo de config forest_whole
3. Seleccionar la capa raster forest
4. Definir la region de muestreo whole map layer
5. Definir el área de muestreo moving window

<br />
{{< figure src="/images/lisetup.png" >}}
<br />

Para el segundo archivo:

1. Click en __Create__
2. Nombrar el archivo de config forest_mov_win
3. Seleccionar la capa raster forest
4. Definir la region de muestreo whole map layer
5. Definir el área de muestreo moving window
6. Definir la forma de la moving window rectangle
7. Y el ancho y largo width =10, height=10

<br />
{{< figure src="/images/lisetup1.png" >}}
<br />

Ahora estamos listos para el análisis:

```
# Edge density
r.li.edgedensity input=forest config=forest_whole output=forest_edge_full

# Shape index
r.li.shape input=forest config=forest_whole output=forest_shape_full

# Patch number
r.li.patchnum input=forest config=forest_whole output=forest_patchnum_full

# mean patch size
r.li.mps input=forest config=forest_whole output=forest_mps_full
```
<br />
<br />

Y hacemos el mismo análisis a partir de el archivo de configuración forest_mov_win :

```
# edge density
r.li.edgedensity input=forest config=forest_mov_win output=forest_edge_mw
# shape index
r.li.shape input=forest config=forest_mov_win output=forest_shape_mw
# patch number
r.li.patchnum input=forest config=forest_mov_win output=forest_patchnum_mw
# mean patch size
r.li.mps input=forest config=forest_mov_win output=forest_mps_mw
 ```

 Debajo de cada análisis sale la carpeta en la que se van almacenando los resultados.

 - Que diferencia hay en los resultados usando el archivo forest_whole y forest_mov_win ?

Para saber más sobre estas herramientas pueden visitar la pagina de [manual](https://grass.osgeo.org/grass74/manuals/r.li.html)

<br />
{{< figure src="/images/programa4.jpg" >}}
<br />
