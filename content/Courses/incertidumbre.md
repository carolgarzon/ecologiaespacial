---
title: "Ejercicio - Mapas de ignorancia"
date: 2018-08-19T15:18:35-05:00
description: "Cartogramas: Mapas de incertidumbre"
featured_image: "/images/programa5.png"
draft: false
type: page
---

El concepto de "mapas de ignorancia" no es nuevo. En 1949, Whittemore discutió este tema en su artículo titulado [_"An atlas of ignorance: A needed stimulus to honest thinking and hard work"_](https://www.jstor.org/stable/3143475?seq=1#page_scan_tab_contents), el cual fue inspiración para la propuesta planteada por Rocchini et al. 2011 en su artículo titulado [_"Accounting for uncertainty when mapping species distributions: The need for maps of ignorance"_ ](http://journals.sagepub.com/doi/abs/10.1177/0309133311399491) donde discute los posibles sesgos que se tienen cuando se hacen modelos espaciales (especialmente modelos de distribución de especies). Y en el articulo de 2017 titulado [_"Anticipating species distributions: Handling sampling effort bias under a Bayesian framework"_](https://www.sciencedirect.com/science/article/pii/S0048969716327231) presenta los cartogramas como un mapa de incertidumbre.

Hoy realizaremos un mapa de incertidumbre para un set de datos de GBIF. Primero aprenderemos a bajar y visualizar datos en R, luego crearemos una grilla en GRASS GIS y finalmente crearemos el cartograma en ScapeToad.


## BAJAR DATOS DE GBIF EN R

Recuerden que pueden importarlos desde la pagina de GBIF. Para bajarlos en R usando los paquetes __dismo__ y __rgbif__ deben consultar la [pagina de GBIF ](https://www.gbif.org)y responder las siguientes preguntas con respecto a su especie seleccionada:

1. Como esta __exactamente__ redactado el nombre de la especie?
2. Esta en Colombia? (Debe estar en Colombia porque esa sera el area de trabajo)
3. Cuantas ocurrencias de esta especie hay en Colombia? (Para hacer el análisis más rápido les aconsejo que no sean más de 200 ocurrencias)

Ahora es importante revisar el tipod e datos que vamos a bajar antes de bajarlos. __Recuerden cambiar el nombre de la especie a la que eligieron__

```
install.packages("dismo", "rgbif", "raster")
library (dismo,rgbif,raster)

occ_search(scientificName = "Astrocaryum standleyanum", return =  "meta")$count

```
Una vez confirmado que el numero de ocurrencias no sobrepase el limite de 200 podemos bajarlas:

```
occ <- occ_search(scientificName = "Astrocaryum standleyanum", limit=200, return = 'data', fields=c('name','basisOfRecord','decimalLatitude','decimalLongitude'), hasCoordinate = T, country = 'CO')
```
Y a partir de esos datos bajados podemos hacer un poco de filtrado y limpieza usando los siguientes comandos:

```
#Eliminar duplicados. TRUE: duplicado, FALSE: unico record

dup_results <- duplicated(occ[c("decimalLongitude","decimalLatitude")])
table(dup_results)

#Saquemos solo los records únicos
occ_unicos <- occ[!dup_results,]
nrow(occ_unicos) #Nuestro numero de records después de quitar los duplicados

#Eliminamos los records con NA (sin coordenadas)
occ_updated <- subset(occ_unicos, (!is.na(decimalLongitude)) | (!is.na(decimalLatitude)) )

#Ahora preparamos los datos para poder verlos en el mapa
coordinates( occ_updated ) <- ~ decimalLongitude + decimalLatitude
class(occ_updated)
```
También podriamos guardar los datos como shapefile o simplemente ver como quedo la tabla:

```
#write.csv(occ,"Astrocaryum_standleyanum.csv") # Si queremos guardar la tabla

#También podemos guardar los datos como shapefile (para verlos en QGIS)
shapefile(occ_updated,"occ.shp")

#Veamos como quedo la tabla
View(occ_updated)
```
Para ver los datos en R podemos usar wrld_simple:

```
data(wrld_simpl)

plot(wrld_simpl, xlim=c(-90,-50), ylim=c(-30,30), axes=TRUE, col='light green', las=1) #plot points on a world map

points(occ_updated, col='orange', pch=20, cex=0.75)

```
o con la base de datos GDAM:

```
col<- getData('GADM', country='COL', level=0)
shapefile(col,"colombia.shp")
plot(col)
points(occ_updated, pch=19, cex=0.5, col="blue")

```

En este punto ya tenemos nuestros datos bajados en formato shapefile. Podemos crear la grilla en GRASS GIS

## EN GRASS GIS

Podemos usar la Localización WGS84 y el mapset con el que hemos venido trabajando. Como ya convertimos la tabla de puntos a shapefile la podemos importar usando el codigo de importacion de vectores:

```
v.in.ogr -o --overwrite input=occ.shp                        

v.in.ogr -o --overwrite input=colombia.shp                        

```
No olviden usar d.vect para ver agregar los mapas al administrador de capas y si hacen click derecho sobre la capa de ocurrencias y seleccionan Propiedades podran cambiar el color de los puntos.

<br />
{{< figure src="/images/occMapa.png" >}}
<br />

Recuerden establecer la capa __colombia__ como la region computacional. Ahora debemos crear una grilla que cubre el área del pais:

```
v.mkgrid --overwrite map=grid box=0.5,0.5                           
```
<br />
{{< figure src="/images/gridMapa.png" >}}
<br />

Ahora debemos crear una columna en la grilla en la que pondremos el cálculo del número de puntos por celda.

```
# Agregar la columna donde va el número de puntos
v.db.addcolumn map=gridcolumns="puntos INT"                          

# Contar los puntos en cada celda
v.vect.stats points=occ areas=grid count_column=puntos  
```
Ahora separaremos en dos grupos las celdas, una con los valores mayores a 0 (celdas con observaciones) y otro con la grilla completa.

```
v.extract --overwrite input=grid@carolgarzon where=puntos > 0 output=grid1      
```

Esta capa la podemos exportar como shapefile.

```
v.out.ogr input=grid output=/Users/carolgarzon/grid.shp format=ESRI_Shapefile
v.out.ogr input=grid1 output=/Users/carolgarzon/grid1.shp format=ESRI_Shapefile
```
<br />

# EN ScapeToad

Pueden bajar el programa de ScapeToad de [https://scapetoad.choros.ch/download.php](https://scapetoad.choros.ch/download.php) recuerden que deben tener Java instalado para poder correr el programa.

Después de instalado deben abrirlo desde el archivo de ScapeToad con extension .jar

<br />
{{< figure src="/images/scape1.png" >}}
<br />

En la ventana de ScapeToad deben seleccionar __Add Layer__

<br />
{{< figure src="/images/scape2.png" >}}
<br />

Luego de agregar las capas de grid y de grid1 deben hacer click en __Create cartogram__ y deben seguir una serie de 6 pasos.

1. Spatial coverage: grid1
2. Cartogram attribute: puntos - Attribute type: density
3. Layers to transform, select: grid

Y compute!

<br />
{{< figure src="/images/scapeResult.png" >}}
<br />

Finalmente usando la opcion Export to shape pueden guardar los cartogramas como shapefiles.

Y por si falta algun otro programa por explorar...

# EN QGIS!

An este programa lo que haremos es poner bonito el mapa. Para eso debemos iniciar con un Nuevo Proyecto e importar el cartograma y el mapa de colombia. Una vez importados tenemos la opcion de usar una Mask (plugin Mask) para ver solo el area de Colombia y cambiar los colores para ver el gradiente.

<br />
{{< figure src="/images/QGIS.png" >}}
<br />

## FIN!
