---
title: "Ejercicio - Series de tiempo"
date: 2018-08-10T15:18:35-05:00
description: "Procesamiento de imagenes en series de tiempo en GRASS GIS"
featured_image: "/images/programa6.png"
draft: false
type: page
---

De las imagenes satelitales se puede extrar variables ambientales determinantes y limitantes para la estructura y distribución de las comunidades. En el ejercicio de hoy aprenderemos a trabajar con datos satelitales en series de tiempo, utilizando herramientas de GRASS GIS.

Hay varios satélites que constantemente proveen imagenes y uno de ellos es MODIS. MODIS significa Moderate Resolution Imaging Spectroradiometer y fue lanzado por primera vez en 1999. Tiene resolución espacial alta y pasa cada dos dias por el mismo lugar.

<br />
{{< figure src="/images/MODIS.jpg" >}}
<br />

Los datos de MODIS se pueden bajar gratis de este [lugar](https://earthdata.nasa.gov), después de registrarse.

## PREPARACIÓN

Para el trabajo de hoy debemos bajar los datos de [AQUI](https://gitlab.com/veroandreo/grass-gis-geostat-2018/blob/master/data/modis_lst.zip) y los deben descomprimir y guardar en la carpeta como una carpeta en el folder nc_spm_08_grass7 en grassdata.

Además debemos bajar la libreria de pyModis para poder usar la extension que necesitaremos en GRASS GIS. La libreria de pyModis la pueden bajar de [AQUI](http://www.pymodis.org/info.html#requirements) es más fácil si la instalan usando _pip_. Y la implementación para Python _ply_

Pueden hacerlo usando el sigueinte codigo en la ventana de terminal:

```
pip install pyModis
pip install ply
```

# EN GRASS GIS

Una vez hecho esto pueden iniciar GRASS GIS y seleccionar __nc_spm_08_grass7__ de Localización y __modis_lst__ de mapset.

Debemos instalar la extension _i.modis_

```
g.extension extension=i.modis
```
AHora veamos la lista de rasters que hay en este mapset:

```
g.list type=raster
```
Y veamos las caracteristicas de una de las capas de raster:

```
r.info map=MOD11B3.A2015060.h11v05.single_LST_Day_6km
```
La sigls LST hace referencia a Lands Surface Temperature. Los datos con los que vamos a trabajar conforman una serie de tiempo de Temperatura terrestre superficial que abarcan desde el 1 de Enero de 2015 hasta el 1 de Enero de 2018.

Ahora debemos establecer region, resolución y mascara:

```
# Establecer la region dentro del estado de NC y con la resolución de los mapas LST
g.region -p vector=nc_state align=MOD11B3.A2015060.h11v05.single_LST_Day_6km

# Crear mascara usando el vector nc_state como limite
r.mask vector=nc_state
```
A partir de ahora aparecerá el texto __[Raster MASK present]__ en el terminal, en cada operación que hagan.


Ahora debemos el STRDS que hacen referencia a la Space Time Raster Dataset, un grupo de imagenes que se trabajaran como grupo de STRDS de aqui en adelante.

```
# Crear STRDS
t.create type=strds temporaltype=absolute output=LST_Day_monthly title="Monthly LST Day 5.6 km" description="Monthly LST Day 5.6 km MOD11B3.006, 2015-2017" --o

# Revisar que se hayan creado las SRTDS correctamente.
t.list type=strds

# Obtener información sobre las STRDS
t.info input=LST_Day_monthly
```

Ahora debemos registrar el tiempo (time stamp) de las STRDS

```
# En sistemas MAC y LINUX
t.register -i input=LST_Day_monthly maps=`g.list type=raster pattern="MOD11B3*LST_Day*" separator=comma` start="2015-01-01" increment="1 months"

# en MS Windows, Primero debemos crear la lista de mapas
g.list type=raster pattern="MOD11B3*LST_Day*" output=map_list.txt

# Y luego lo podemos registrar
t.register -i input=LST_Day_monthly file=map_list.txt start="2015-01-01" increment="1 months"

# Revisar la información resultante
t.info input=LST_Day_monthly

# Revisar la lista de mapas en STRDS
t.rast.list input=LST_Day_monthly

# Revisar el minimo y maximo para cada mapa
t.rast.list input=LST_Day_monthly columns=name,min,max

# Grafiquemos ese cambio
g.gui.timeline inputs=LST_Day_monthly

```

Para hacer los siguientes calculos primero debemos pasar de grados Kelvin (el sistema de temperatura en el que los datos estan orginalmente) a grados centigrados.

```
# Transformar los datos a Celsius
t.rast.algebra basename=LST_Day_monthly_celsius expression="LST_Day_monthly_celsius = LST_Day_monthly * 0.02 - 273.15"

# Revisar la información resultante
t.info LST_Day_monthly_celsius
```
A partir de los mapas podemos obtener algunas gráficas:


```
# LST time series plot for the city center of Raleigh
g.gui.tplot strds=LST_Day_monthly_celsius coordinates=670245.988279, 157316.183595
```

También podemos obtener una lista especifica de mapas que cumplan con un set de caracteristicas:


```
# Mapas con valor minimo de temperatura menor o igual a 5
t.rast.list input=LST_Day_monthly_celsius order=min columns=name,start_time,min where="min <= '5.0'"

# Mapas con un maximo mayor a 30
t.rast.list input=LST_Day_monthly_celsius order=max columns=name,start_time,max where="max > '30.0'"

# Mapas dentro de un rango de tiempo específico
t.rast.list input=LST_Day_monthly_celsius columns=name,start_time where="start_time >= '2015-05' and start_time <= '2015-08-01 00:00:00'"

# Mapas de Enero
t.rast.list input=LST_Day_monthly_celsius columns=name,start_time where="strftime('%m', start_time)='01'"

```

### Algunas operaciones estadisticas descriptivas

Podemos ver las estadisticas univariadas de los mapas:

```
t.rast.univar input=LST_Day_monthly_celsius
```

Tambien podemos ver el set de analisis estadisticos completo y salvarlo en un archivo csv:

```
# Estadisticas
t.rast.univar -e input=LST_Day_monthly_celsius

# Guardar en un archivo csv
t.rast.univar input=LST_Day_monthly_celsius separator=comma output=stats_LST_Day_monthly_celsius.csv
```

Es posible hacer agregaciones temporales:

```
# Obtener el maximo LST de los STRDS
t.rast.series input=LST_Day_monthly_celsius output=LST_Day_max method=maximum

# Obtener el minimo de los STRDS
t.rast.series input=LST_Day_monthly_celsius output=LST_Day_min method=minimum

# Cambiar el color de la paleta a Celsius
r.colors map=LST_Day_min,LST_Day_max color=celsius
```

EN GRASS GIS hay una función muy interesante (y útil para visualización que se llama mapswipe. Vamos a usarla para ver la relación entre la temperatura (LST) maxima y minima y la elevación:

```
# LST_Day_max & elevation
g.gui.mapswipe first=LST_Day_max second=elev_state_500m

# LST_Day_min & elevation
g.gui.mapswipe first=LST_Day_min second=elev_state_500m
```

Las variables temporales permiten otro tipo de operaciones:

 ```
# Para obtener el mes con el maximo LST
t.rast.mapcalc -n inputs=LST_Day_monthly_celsius output=month_max_lst expression="if(LST_Day_monthly_celsius == LST_Day_max, start_month(), null())" basename=month_max_lst

# Obtener información básica
t.info month_max_lst

# Obtener el primer mes en el que la temperatura maxima apareció (metodo minimo)
t.rast.series input=month_max_lst method=minimum output=max_lst_date

# remover month_max_lst strds porque solo nos interesa el mapa agregado
t.remove -rf inputs=month_max_lst
```
Y para ver el resultado en una ventana nueva:

```
# Abrir una ventana
d.mon wx0

# Visualizar el mapa raster
d.rast map=max_lst_date

# Agregar el borde del estado de NC
d.vect map=nc_state type=boundary color=#4D4D4D width=2
```


## Agregación temporal (granularidad de cada tres meses)

```
# LST promedio de cada 3 meses
t.rast.aggregate input=LST_Day_monthly_celsius output=LST_Day_mean_3month basename=LST_Day_mean_3month suffix=gran method=average granularity="3 months"

# Revisar el resultado
t.info input=LST_Day_mean_3month

# Ver la lista de mapas creados
t.rast.list input=LST_Day_mean_3month
 ```

Ahora podemos crear un increible gif a partir de los mapas de agregación de cada 3 meses:

```
# Establecer el color de los STRDS a celsius
t.rast.colors input=LST_Day_mean_3month color=celsius

# Iniciar una nueva ventana de display en la cual se veran los mapas /tmp/map.png con imagenes de tamaño 640x360px
d.mon cairo out=frames.png width=640 height=360 resolution=4

# crear el primer frame
d.frame -c frame=first at=0,50,0,50
d.rast map=LST_Day_mean_3month_2015_07
d.vect map=nc_state type=boundary color=#4D4D4D width=2
d.text text='Jul-Sep 2015' color=black font=sans size=10

# crear el segundo frame
d.frame -c frame=second at=0,50,50,100
d.rast map=LST_Day_mean_3month_2015_10
d.vect map=nc_state type=boundary color=#4D4D4D width=2
d.text text='Oct-Dec 2015' color=black font=sans size=10

# crear el tercer frame
d.frame -c frame=third at=50,100,0,50
d.rast map=LST_Day_mean_3month_2015_01
d.vect map=nc_state type=boundary color=#4D4D4D width=2
d.text text='Jan-Mar 2015' color=black font=sans size=10

# crear el cuarto frame
d.frame -c frame=fourth at=50,100,50,100
d.rast map=LST_Day_mean_3month_2015_04
d.vect map=nc_state type=boundary color=#4D4D4D width=2
d.text text='Apr-Jun 2015' color=black font=sans size=10

# salir del display
d.mon -r

# La animación de LST
g.gui.animation strds=LST_Day_mean_3month
```

et voilà!
