---
title: "Extras - GoogleEarth Pro"
date: 2018-08-01T15:18:35-05:00
description: "Algunas herramientas en Google Earth"
featured_image: "/images/programa8.png"
draft: false
type: page
---

Google Earth Pro nos da acceso a imagenes satelitales en varias resoluciones y a traves del tiempo y nos ofrece un set de herramientas básicas para manejo de datos y análisis.

Para bajar Google Earth Pro pueden ir a: [https://www.google.com/earth/download/gep/agree.html](https://www.google.com/earth/download/gep/agree.html) y seleccionar la aplicación que corresponde a su sistema operativo.

Google Earth Pro tiene varias aplicaciones, a continuación un video:

<iframe width="560" height="315" src="https://www.youtube.com/embed/mZGniniFLzs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Personalización

Una vez instalado y abierto Google Earth podemos empezar a explorar y seleccionar nuestras preferencias con respecto a las unidades de medida y la visualización de las capas raster. Para esto debemos hacer click en Google Earth Pro (Mac) o Herramientas (Windows) y elegir Preferencias o Opciones. Se abrirá una ventana como esta:

<br />
{{< figure src="/images/googlePref.png" >}}
<br />

En esta ventana podemos seleccionar la unidad en la que vemos las coordenadas y la unidad con la que se miden distancias y áreas.

## Crear polígonos

En la parte superiror de la ventana de Google Earth Pro hay una serie de iconos con varias funciones:

<br />
{{< figure src="/images/GoogleMenu.png" >}}
<br />

Para crear polígonos y exportarlos debemos:

1. Hacer click sobre el icono de crear polígonos
2. Se abre una ventana nueva y si ponemos el cursor sobre el mapa tendra forma cuadrada.
<br />
{{< figure src="/images/googlePol.png" >}}
<br />

3. Para crear el poligono debemos hacer click sobre el mapa como uniendo puntos que iran creando la forma del poligono. Por ejemplo en esta imagen estamos delineando la forma de la zona verde del bobo en la universidad.

<br />
{{< figure src="/images/googlePol1.png" >}}
<br />

4. Una vez terminada la forma del polígono le agregamos un nombre en la ventana y aceptar.
5. En la barra al lado izquierdo de la pantalla aparecerá el polígono creado.
6. Para guardar el polígono creado debemos hacer click derecho sobre el nombre en la barra del lado izquierdo y seleccionar _salvar lugar como_
7. El archivo se guardará con extension __.kmz__
8. Este tipo de archivos pueden ser abiertos en QGIS y guardados como shp para ser usados en R o en GRASS.
<br />
<br />

## Esperen más herramientas!






## MODELOS DE DISTRIBUCION DE ESPECIES

Un tema central en ecología es explicar como la biodiversidad esta espacial y temporalmente distribuida en el planeta, y por lo tanto, las metodologías desarrolladas para abarcar este tema se han vuelto esenciales para el monitoreo de la biodiversidad. Los determinantes más importantes en esta distribución de especies se han agrupado en dos categorias principales:



### Condiciones ambientales
En la naturaleza, las especies tienen un set de requerimientos específicos que limitan su distribución, y por consiguiente cada especies (o grupo de especies) ocupan el espacio que coincide con el set de condiciones que necesitan.

Las variables ambientales vienen de multiples fuentes: estaciones meteorológicas, sensores remotos, mediciones _in situ_, entre otras. Y su seleccion depende de:  

- Caracteristicas de las especies (tamaño, movimiento, dispersion) y su ecología (interacciones, estrategias, historia de vida).
- Caracteristicas de los datos ambientales (rango temporal, extensión espacial, variabilidad, resolución espacial).
- Caracteristicas de los datos de observaciones directas (rango temporal, extensión espacial, metodología de muestreo, esfuerzo de muestreo)



### Caracteristicas ecológicas e interacciones

La fuerza de la asociación entre una especie y el ambiente físico es modificada por las interacciones bióticas, el tipo de dispersion o rango de hogar y su habilidad para persistir en su ambiente.

Las interacciones bioticas pueden ser muy complejas, pueden variar entre beneficios o antagonistas, directas o indirectas, y también de pueden ser más fuertes o debilitarse de acuerdo con el contexto espacial en el que se llevan a cabo. Por todo lo anterior, la distribución de las especies no se puede predecir unicamente con base en variables ambientales, tienen que incluir el componente biótico como determinante fundamental de la distribución.

## Modelos de distribución de especies ó Modelos de nicho ecológico?

Este debate ha estado presente entre grupos de científicos que basan esta decision en el alcance del modelo. Partiendo de esa condición es relativamente fácil saber si se esta modelando la distribución de una especie o su nicho. Para la cual es requisito fundamental tener claros los conceptos de __nicho__, la diferencia entre __nicho fundamental (potencial) y nicho realizado (actual)__ y saber identificar correctamente lo que describe cada modelo especifico ([McInerny & Ettiene, 2012](https://onlinelibrary.wiley.com/doi/abs/10.1111/jbi.12031)).

En este sentido, un modelo de distribución incluye todas las metodologias que se usan para predecir la distribución de una especie, a partir solo de la correlación entre variables ambientales y observaciones hasta el desarrollo de modelos que parten del conocimiento sobre la especie y su ecología. Y en este rango se incluirian los modelos de nicho ecológico.

## Pasos para realizar un modelo (SDM - Species Distribution Models):

### Observaciones:

La fuente de las observaciones que usamos para modelar la distribución de una especie varia en número de observaciones, extensión temporal y espacial, y en el metodo y esfuerzo de muestreo. Pueden ser a partir de datos colectados por nosotros o con base en observaciones almacenadas en bases de datos en linea como GBIF, entre otras. Para bajar los datos de GBIF podemos seguir estos pasos:

```


```




Con base en el conjunto de observaciones debemos ser capaces de contestar las siguientes preguntas:

- ¿Que se sabe de la ecología de la especie para la que se va hacer el SDM?
- ¿Hay observaciones directas de individuos de esa especie?
- ¿Son solo presencias? ¿Hay ausencias y presencias disponibles?
- ¿Que metodo (o metodos) de muestreo se usaron para colectar las observaciones?
- ¿Cual es el rango temporal y espacial que cubren las observaciones?
- ¿Hay duplicados?
- ¿Cual es el sistema de coordenadas en el que estan las observaciones?
- ¿Hay observaciones sin coordenadas geográficas?
- ¿Las coordenadas geograficas de las observaciones son precisas?
- ¿Hay observaciones en rangos donde la especie no puede vivir (especie marina - observación en ecosistema terrestre)?
