---
title: "Ejercicio GRASS + R"
date: 2019-09-18T15:18:35-05:00
description: "Procesamiento de capas en GRASS y análisis de autocorrelación espacial en R"
featured_image: "/images/programa.png"
draft: false
type: page
---
En este ejercicio crearemos (o utilizaremos una localización ya creada) una localización con la projección WGS 84 (codigo EPGS:4326) con su respectivo mapset e importaremos capas de raster (CHELSA climate) y vector (ocurrencias de Attalea butyraceae GBIF y puntos al azar con abundancias de Attalea). La primera parte de este ejercicio se realizará en GRASS GIS y la segunda parte en R.


## Preprocesamiento en GRASS GIS

```
g.region -p

d.mon wx0  #Abrir una ventana de display si estan trabajando desde el terminal
```
### Importar capas raster CHELSA

Capas de Temperatura (Bio1) y Precipitación (Bio12) de Bioclim. Antes de importar estas cpaas debemos hacer una correción porque estan por fuera de los limites geográficos, asi que debemos reparar la extensión del raster usando el comando _gdal_translate_ ,
para luego importarla en grass usando el comando _r.import_.

```
gdal_translate -a_ullr -180 90 180 -60 CHELSA_bio10_1.tif bio1_fixed.tif
r.import input= "bio1_fixed.tif" output=bioclim01 resample=bilinear extent=region resolution=region -n

gdal_translate -a_ullr -180 90 180 -60 CHELSA_bio10_12.tif bio12_fixed.tif
r.import input= "bio12_fixed.tif" output=bioclim12 resample=bilinear extent=region resolution=region -n
```

### Importar vector gbif
```
v.in.gbif --overwrite input=C:\Users\cx.garzon\Documents\0013052-180824113759888.csv output=AttaleaGBIF
```
### Importar puntos
```
v.in.ogr input=C:\Users\cx.garzon\Documents\puntos.shp output=Puntos
```
### Ver las capas importadas
```
d.rast map=CHELSA_bio1
d.rast map=CHELSA_bio12
d.vect map=AttaleaGBIF
g.region -p
```
### Establecer la region
```
g.region vector=AttaleaGBIF
```
### Crear un poligono del área de estudio
```
v.mkgrid --overwrite map=areaEstudio grid=1,1                                  
```
### Crear una mascara para establecer el área en la que se haran los calculos
```
r.mask vector=areaEstudio                                          
```
### Cortar los rasters al área de estudio
```
r.mapcalc expression=Bio1_america = CHELSA_bio1                      
r.mapcalc expression=Bio12_america = CHELSA_bio12
```
### Eliminar la máscara

__Importante! Siempre recordar que la máscara esta activa al hacer operaciones porque todo lo que hagamos solo se verá reflejado dentro de la máscara!__

```
r.mask -r
```
<br />
<br />

## EN R
```
library(raster)
library(virtualspecies)
library(rgrass7)
library(mapview)
```

### Configurar GRASS en R
```
myGRASS <- "C:/Program Files/GRASS GIS 7.4.0"  #Set the location of the grass binaries and libraries
myGISDbase <- "C:/Users/cx.garzon/Documents/grassdata/"  #Set the directory path to your GISDbase
myLocation <- "GBIF"  #Name the Location
myMapset <- "cx.garzon"  #Name the mapset
```
### Iniciar la sesion de GRASS GIS
```
initGRASS(gisBase = myGRASS, home = tempdir(), gisDbase = myGISDbase, location = myLocation,
          mapset = myMapset, override = TRUE)
```
### Establecer la region
```
execGRASS("g.region", flags=c("p"), vector="areaEstudio")
```
### Leer capas raster y vector
```
BIO1<-readRAST("Bio1_America")                                                                                                                                      

BIO12<-readRAST("Bio1_America")

Attalea <- readVECT("Attalea1GBIF")
Puntos<- readVECT("Puntos")

mapview(BIO1) + BIO12 + Attalea + Puntos
```

{{< figure src="/images/mapviewAmerica.png" >}}

### Analisis de Autocorrelacion
```
install.packages("spdep","vegan")
library(spdep)
library(vegan)
```
### Extraer las coordenadas de los datos
```
xy <- data.frame(Puntos@coords)
```
### distancias entre puntos
```
xy.dist <- dist(xy)
```

### distancias entre puntos
```
r.nb <- dnearneigh(as.matrix(xy), d1=0.5, d2=1.5)
zvalue<- Puntos@data[,1]
```

### Correlograma
```
sp.cor <- sp.correlogram(r.nb, zvalue, order=15,
                         method="I", randomisation=FALSE, zero.policy=TRUE)
```
<br />
<br />
