---
title: "Ejercicio - Análisis de conectividad"
date: 2018-08-12T15:18:35-05:00
description: "Análisis de conectividad en R"
featured_image: "/images/programa7.png"
draft: false
type: page
---

Para este ejercicio usaremos el raster de usos del suelo con el que trabajamos en el ejercicio pasado. Así que primero debemos entrar a GRASS GIS, en localización __nc_spm_08_grass7__ y el mapset en el que estabamos trabajando. Una vez iniciado GRASS GIS podemos exportar la capa _landclass96_ usando el siguiente comando:

_Recuerden cambiar el output a la carpeta en la que quieran guardar su capa raster._

```
r.out.gdal --overwrite input=landclass96@landsat output=/Users/carolgarzon/Desktop/Dataset/landclass96.tiff format=GTiff
```

En este ejercicio vamos a trabajar en __R__ con el paquete __grainscape__, desarrollado por Paul Galpen y Alex M. Chubaty,que permite realizar un análisis de a partir de la teoria de análisis de redes. Es importante recordar que en esta metodología se estudia la conectividad independiente de las caracteristicas de cada fragmento.

Para empezar debemos instalar algunos paquetes:

```
install.packages("devtools")
install.packages("knitr")
install.packages("igraph")
install.packages("raster")

library(ggplot2)
library(knitr)
library(igraph)
library(raster)
library(grainscape)
library(devtools)

devtools::install_github("achubaty/grainscape")
```

No olvidemos establecer la carpeta en la que vamos a guardar los resultados y en la que tenemos almacenada la capa _landclass96_, e importar este raster.

```
setwd("//Users/carolgarzon/Desktop/Dataset")
getwd()

files<-list.files("/Users/carolgarzon/Desktop/Dataset",full.names=TRUE)
landclass<-raster(files[[1]])  
plot(landclass)
```

Ya podemos ver en el raster de landclass en la ventana de R:
<br />
{{< figure src="/images/landclass.png" >}}
<br />

Ahora vamos a contruir la grafica plana minima (minimum planar graph o MPG) que permite visualizar la red de conexiones en la matriz de fragmento. La construcción de esta red se basa en la movilidad de los organismos a través de la misma, es decir, asume que algunos tipos de parches ofrecen menor o mayor resistencia al paso de los organismos. Por ejemplo, los cuerpos de agua tendrian una alta resistencia (mayor costo) para organismos terrestres pero no es el mismo caso para aves.

En ese sentido, el investigador debe generar un criterio que le permita identificar la resistencia de cada tipo de parche en la matriz de fragmentación, dandole valor menor (1) al ecosistema que constituye el habitat natural del organismo de estudio (bosque en este ejercicio), el mayor valor de resistencia (10) a las zonas que representan la barrera más infranqueable para los organismos (en este caso los cuerpos de agua) y valores intermedios a zonas que presenten obstaculos con diferentes grados de resistencia (en este caso las arbustales: 2, zonas herbaceas: 3 , zonas agrícolas: 4, sedimentos: 5 y zonas urbanas: 8).

<br />
{{< figure src="/images/landclass_Ley.png" >}}
<br />

El procedimiento para asignar a cada una de las clasificaciones del raster (leyenda en la figura)

```
isBecomes <- cbind(c(1, 2, 3, 4, 5, 6, 7), c(8, 4, 3, 2, 1,10,5))
patchyCost <- reclassify(landclass, rcl = isBecomes)
```

Ahora podemos ver el raster reclasificado usando los comandos del paquete ggplot2:

```
ggplot() +
  geom_raster(data = ggGS(patchyCost),
              aes(x = x, y = y, fill = value)) +
  scale_fill_distiller(palette = "Paired", guide = "legend") +
  guides(fill = guide_legend(title = "Resistance")) +
  theme(legend.position = "right")
```
<br />
{{< figure src="/images/resistance.png" >}}
<br />

Y calcular el MPG con base en los valores de resistencias que asignamos:

```
patchyMPG <- MPG(patchyCost, patch = (patchyCost == 1))

plot(patchyMPG, quick = "mpgPlot", theme = FALSE)
```

El MPG será la base de los siguientes análisis porque brinda una red con todas las posibles conexiones entre los nodos (parches) de la matriz.

También podemos ver la información contenida en el MPG en cuanto a nodos y links:

```
# Nodos:

nodeTable <- graphdf(patchyMPG)[[1]]$v

kable(nodeTable[1:5, ], digits = 0, row.names = FALSE) #EN la tabla 1:5 indica el número de filas a visualizar y podemos cambiar ese valor

# Links:

linkTable <- graphdf(patchyMPG)[[1]]$e

kable(linkTable[1:3, ], digits = 0, row.names = FALSE)
```
En nuestra investigación también puede ser interesante identificar las agregaciones o clusters de áreas conectadas (components). Esto se puede hacer por medio del comando _treshold_, donde nThresh indica el número de clusters (componentes)

```
scalarAnalysis <- threshold(patchyMPG, nThresh = 10)

kable(scalarAnalysis$summary,
      caption = paste("The number of components ('nComponents') in the",
                      "MPG at 10 automatically-selected thresholds")

ggplot(scalarAnalysis$summary, aes(x = maxLink, y = nComponents)) +
  geom_line(colour = "forestgreen") +
  xlab("Link Threshold (resistance units)") +
  ylab("Number of components") +
  scale_x_continuous(breaks = seq(0, 800, by = 100)) +
  scale_y_continuous(breaks = 1:20) +
  theme_light() + theme(axis.title = element_text())

```
<br />
{{< figure src="/images/components.png" >}}
<br />

Ahora usemos esa información de componentes para evaluar la conectividad en la red de parches, con base en unos limites de resistencia tolerables por los organismos. Comparemos una grafica con resistencia maxima de 250 unidades y otra con resistencia maxima de 100 unidades:

```
ggplot() +
  geom_raster(data = ggGS(patchyMPG, "patchId"),
              aes(x = x, y = y, fill = value > 0)) +
  scale_fill_manual(values = "grey") +
  geom_segment(data  = ggGS(patchyMPG, "links"),
               aes(x = x1, y = y1, xend = x2, yend = y2,
                   colour = lcpPerimWeight >= 250)) +
  scale_colour_manual(values = c("forestgreen", NA)) +
  geom_point(data = ggGS(patchyMPG, "nodes"), aes(x = x, y = y),
             colour = "darkgreen")

             ggplot() +
               geom_raster(data = ggGS(patchyMPG, "patchId"),
                           aes(x = x, y = y, fill = value > 0)) +
               scale_fill_manual(values = "grey") +
               geom_segment(data  = ggGS(patchyMPG, "links"),
                            aes(x = x1, y = y1, xend = x2, yend = y2,
                                colour = lcpPerimWeight >= 50)) +
               scale_colour_manual(values = c("forestgreen", NA)) +
               geom_point(data = ggGS(patchyMPG, "nodes"), aes(x = x, y = y),
                          colour = "darkgreen")
```

- Que pasa con la red? Aumentan o disminuyen las conexiones?

## Explorando la matriz a partir de los diagramas de Voronoi

Los diagramas Voronoi permiten crear areas alrededor de puntos (o parches) encerrando todo lo que esta más cerca a ese punto que a otros puntos. SU aplicación en conectividad se basa en generar una representación del area de resistencia (y el valor de resistencia) alrededor de cada parche. Veamos:

```
patchPlusVoronoi <- patchyMPG@voronoi
patchPlusVoronoi[patchyMPG@patchId] <- 0

ggplot() +
  geom_raster(data = ggGS(patchPlusVoronoi), aes(x = x , y = y, fill = value))
```

En el plot resultante vemos como las zonas más claras tienen mayor resistencia que las zonas oscuras. Esto nos puede indicar que parches estan más desconectados que otros.


A partir de ese diagrama de Voronoi podemos crear un GOC o Grains of Connectivity (grados de conectividad), el cual nos indica los poligonos de voronoi que estan más conectados. Los valores son seleccionados con base en un limite establecido (treshold).

```
patchyGOC <- GOC(patchyMPG, nThresh = 10)

plot(grain(patchyGOC, whichThresh = 6), quick = "grainPlot", theme = FALSE)
```
En la grafica resultande vemos las areas que permanecen conectadas a partir de un threshold especifico que determina la capacidad de movimiento.

<br />
{{< figure src="/images/voronoi.png" >}}
<br />

## Algunas visualizaciones de la red de fragmentos

Usando el paquete __grainscape__ y en combinación con el paquete __ggplot2__ podemos obtener graficas que reflejen diferentes caracteristicas de los parches o la matriz que estamos observando.

```
#Tamaño de los parches

NodosTamaño <- ggplot() +
  geom_segment(data = ggGS(patchyMPG, "links"),
               aes(x = x1, y = y1, xend = x2, yend = y2),
               colour = "forestgreen") +
  geom_point(data = ggGS(patchyMPG, "nodes"),
             aes(x = x, y = y, size = patchArea), colour = "darkgreen") +
  scale_size_area(max_size = 10, breaks = c(1000, 3000)) +
  ggtitle("Characteristics of nodes (weights)")

NodosTamaño

# Distancia entre nodos (en relación con la superficie de resistencia)

DistanciaRes <- ggplot() +
  geom_segment(data = ggGS(patchyMPG, "links"),
               aes(x = x1, y = y1, xend = x2, yend = y2,
                   size = lcpPerimWeight / (sqrt((x2 - x1) ^ 2 + (y2 - y1) ^ 2))),
               colour = "forestgreen", alpha = 0.5) +
  scale_size(range = c(0, 3), breaks = seq(1, 6, by = 0.5)) +
  geom_point(data = ggGS(patchyMPG, "nodes"),
             aes(x = x, y = y), size = 3, colour = "darkgreen") +
  ggtitle("Caracteristicas d elas lineas(weights)")

DistanciaRes

# Números asignados a los fragmentos

FragNum<- ggplot() +
  geom_raster(data = ggGS(patchyMPG, "patchId"),
              aes(x = x, y = y, fill = value > 0)) +
  scale_fill_manual(values = "grey") +
  geom_text(data = ggGS(patchyMPG, "nodes"), aes(x = x, y = y, label = name),
            colour = "black", size = 2)
FragNum
```

## Distancia más corta entre fragmentos

En la anterior gráfica vimos los números asignados a cada fragmento. Ahora vamos a calcular la distancia entre dos fragmentos asignados.

```
startEnd <- c(34, 9)

shPath <- shortest_paths(patchyMPG$mpg,
                         from = which(V(patchyMPG$mpg)$patchId == startEnd[1]),
                         to = which(V(patchyMPG$mpg)$patchId == startEnd[2]),
                         weights = E(patchyMPG$mpg)$lcpPerimWeight,
                         output = "both")

## Extracción de nodos y uniones entre parches
shPathN <- as.integer(names(shPath$vpath[[1]]))
shPathL <- E(patchyMPG$mpg)[shPath$epath[[1]]]$linkId

## Tabla correspondiente a los nodos y uniones de camino más corto
shPathNodes <- subset(ggGS(patchyMPG, "nodes"), patchId %in% shPathN)
shPathLinks <- subset(ggGS(patchyMPG, "links"), linkId %in% shPathL)

## Calcular la distancia del camino más corto
shPathD <- distances(patchyMPG$mpg,
                     v = which(V(patchyMPG$mpg)$patchId == startEnd[1]),
                     to = which(V(patchyMPG$mpg)$patchId == startEnd[2]),
                     weights = E(patchyMPG$mpg)$lcpPerimWeight)[1]

## Graficar el resultado
PathsFrag <- ggplot() +
  geom_raster(data = ggGS(patchyMPG, "patchId"),
              aes(x = x, y = y,
                  fill = ifelse(value %in% shPathN, "grey70", "grey90"))) +
  scale_fill_identity() +
  geom_segment(data  = shPathLinks, aes(x = x1, y = y1, xend = x2, yend = y2),
               colour = "forestgreen", size = 1) +
  geom_point(data = shPathNodes, aes(x = x, y = y), colour = "darkgreen") +
  ggtitle("Shortest-path distance between nodes")

PathsFrag

```

Pueden consultar más sobre el paquete __grainscape__ en: [http://www.alexchubaty.com/grainscape/articles/grainscape_vignette.html#introduction](http://www.alexchubaty.com/grainscape/articles/grainscape_vignette.html#introduction)

## FIN!
