---
title: "Ejercicio - SDMs"
date: 2018-07-15T15:18:35-05:00
description: "Modelos de distribución de especies"
featured_image: "/images/programa9.png"
draft: false
type: page
---
#### Preparación

Antes de empezar el ejercicio de hoy debemos instalar los siguientes paquetes de R:

```
install.packages("dismo")
install.packages("biomod2")
install.packages("raster")
install.packages("rgbif")

library(dismo)
library(raster)
library(biomod2)
library(rgbif)
```
También debemos instalar Maxent y para usarlo dentro de su funcionalidad en Biomod2 y Dismo es importante reubicar el archivo __maxent.jar__. Instalemos maxent en el computador bajandolo de esta pagina: [https://biodiversityinformatics.amnh.org/open_source/maxent/](https://biodiversityinformatics.amnh.org/open_source/maxent/)

En el archivo zip que bajamos de maxent podemos encontrar el archivo _maxent.jar_ el cual debemos añadir a la carpeta java de dismo y a la de biomod2

```
#Para localizar la carpeta java en dismo:
system.file('java', package="dismo")
system.file('java', package="biomod2")
```
Bajemos y carguemos en R las capas ambientales y el set de puntos que usaremos para este ejercicio.

```
envData<-getData('worldclim',var='bio',res=10)
envData1<-crop(envData,extent(-100,-30,-60,20)) #Cortar solo el area de suramerica
plot(envData1[[c("bio1", "bio12")]]) # Plot de las variables 1 y 12

#Unificar sistemas de coordenadas
  # Here I directly assign WGS84 to occurrences; but if you have different CRS, you need to transform one to the other.

#Establecer la carpeta de donde salen los datos y donde se guardaran los resultados

setwd()
getwd()

#Cargar datos de presencias

#Podemos usar los datos de GBIF
occ_search(scientificName = "Bradypus variegatus", return =  "meta")$count

bomb.occ <- occ_search(scientificName = "Bradypus variegatus", limit=200, return = 'data', fields=c('name','basisOfRecord','decimalLongitude','decimalLatitude'), hasCoordinate = T, country = 'CO')

coordinates( bomb.occ ) <- ~ decimalLongitude + decimalLatitude
class(bomb.occ)

shapefile(bomb.occ,"occ.shp", overwrite=T)
```

__¡IMPORTANTE! Usaremos datos de gbif y de bioclim porque es fácil obtenerlos pero recuerden que es fundamental seguir el proceso de filtrado y curado de los datos y seleccionar la fuente de datos de mejor calidad y que vaya en acordancia con la resolución y extensión temporal y espacial de nuestras observaciones y la ecología de nuestra especie de estudio.__

## MODELOS DE DISTRIBUCION DE ESPECIES

Un tema central en ecología es explicar como la biodiversidad esta espacial y temporalmente distribuida en el planeta, y por lo tanto, las metodologías desarrolladas para abarcar este tema se han vuelto esenciales para el monitoreo de la biodiversidad. Los determinantes más importantes en esta distribución de especies se han agrupado en dos categorias principales:



### Condiciones ambientales
En la naturaleza, las especies tienen un set de requerimientos específicos que limitan su distribución, y por consiguiente cada especies (o grupo de especies) ocupan el espacio que coincide con el set de condiciones que necesitan.

Las variables ambientales vienen de multiples fuentes: estaciones meteorológicas, sensores remotos, mediciones _in situ_, entre otras. Y su seleccion depende de:  

- Caracteristicas de las especies (tamaño, movimiento, dispersion) y su ecología (interacciones, estrategias, historia de vida).
- Caracteristicas de los datos ambientales (rango temporal, extensión espacial, variabilidad, resolución espacial).
- Caracteristicas de los datos de observaciones directas (rango temporal, extensión espacial, metodología de muestreo, esfuerzo de muestreo)



### Caracteristicas ecológicas e interacciones

La fuerza de la asociación entre una especie y el ambiente físico es modificada por las interacciones bióticas, el tipo de dispersion o rango de hogar y su habilidad para persistir en su ambiente.

Las interacciones bioticas pueden ser muy complejas, pueden variar entre beneficios o antagonistas, directas o indirectas, y también de pueden ser más fuertes o debilitarse de acuerdo con el contexto espacial en el que se llevan a cabo. Por todo lo anterior, la distribución de las especies no se puede predecir unicamente con base en variables ambientales, tienen que incluir el componente biótico como determinante fundamental de la distribución.

## Modelos de distribución de especies ó Modelos de nicho ecológico?

Este debate ha estado presente entre grupos de científicos que basan esta decision en el alcance del modelo. Partiendo de esa condición es relativamente fácil saber si se esta modelando la distribución de una especie o su nicho. Para la cual es requisito fundamental tener claros los conceptos de __nicho__, la diferencia entre __nicho fundamental (potencial) y nicho realizado (actual)__ y saber identificar correctamente lo que describe cada modelo especifico ([McInerny & Ettiene, 2012](https://onlinelibrary.wiley.com/doi/abs/10.1111/jbi.12031)).

En este sentido, un modelo de distribución incluye todas las metodologias que se usan para predecir la distribución de una especie, a partir solo de la correlación entre variables ambientales y observaciones hasta el desarrollo de modelos que parten del conocimiento sobre la especie y su ecología. Y en este rango se incluirian los modelos de nicho ecológico.

## Modelo (SDM - Species Distribution Models):

### Observaciones:

La fuente de las observaciones que usamos para modelar la distribución de una especie varia en número de observaciones, extensión temporal y espacial, y en el metodo y esfuerzo de muestreo. Pueden ser a partir de datos colectados por nosotros o con base en observaciones almacenadas en bases de datos en linea como GBIF, entre otras.

Con base en el conjunto de observaciones debemos ser capaces de contestar las siguientes preguntas:

- ¿Que se sabe de la ecología de la especie para la que se va hacer el SDM?
- ¿Hay observaciones directas de individuos de esa especie?
- ¿Son solo presencias? ¿Hay ausencias y presencias disponibles?
- ¿Que metodo (o metodos) de muestreo se usaron para colectar las observaciones?
- ¿Cual es el rango temporal y espacial que cubren las observaciones?
- ¿Hay duplicados?
- ¿Cual es el sistema de coordenadas en el que estan las observaciones?
- ¿Hay observaciones sin coordenadas geográficas?
- ¿Las coordenadas geograficas de las observaciones son precisas?
- ¿Hay observaciones en rangos donde la especie no puede vivir (especie marina - observación en ecosistema terrestre)?

### Datos ambientales

Los datos ambientales pueden venir de multiples fuentes (satelite, estaciones metereologicas, etc). Es importante conocer la información sobre como estas capas fueron creadas (metadatos) pra asi poder usarlas adecuadamente.

Con base en las variables ambientales seleccionadas debeemos ser capaces de contestar las siguientes preguntas:

- ¿Cual es la resolución de la base de datos seleccionada?
- ¿De donde provienen los datos utilizados para crear esta capa?
- ¿Se ha reportado algún problema para el uso de estas capas para mi tipo de ecosistema(s)?
- ¿Las variables ambientales seleccionadas reflejan los requerimientos de las especies?
- ¿La resolución y extension de la capa se ajusta a la escala espacial del estudio, la ecología de la especie y la pregunta de investigación?
- ¿Las variables ambientales seleccionadas no tienen una alta correlación que aumente los problemas de multicolinearidad en el modelo?
- ¿Todas las capas estan en el mismo sistema de cordenadas?

### Ausencias

La calidad de las ausencias son tan importantes en el desarrollo de un modelo como las presencias, pero la mayoria de los datos solo corresponden a datos de presencia (aunque hay algunos estudios con datos de ausencias), porque para generar un modelo no solo es importante saber donde se encuentra la especie (presencia) si no también donde no esta (ausencia). Y debido a su importancia, su seleccion puede tener grandes consecuencias en la modelación ([Lobo et al. 2010](https://onlinelibrary.wiley.com/doi/abs/10.1111/j.1600-0587.2009.06039.x)).

__IMPORTANTE. Algoritmos como Maxent (aptos para solo presencias) tambien seleccionan las pseudoausencias y las llaman "background data"__

Hay tres criterios para generar ausencias: 1) A una distancia minima de las presencias, 2) Dentro del ecosistema de las presencias, 3) Lejos de las presencias pero dentro del ecosistema de las ausencias.

Crearemos las ausencias usando QGIS siguiendo estos pasos:

1. Importar datos de presencias.
2. Crear area buffer alrededor de las presencias
3. Crear convex hull alrededor de las presencias
4. Crear puntos al azar (igual al número de presencias) dentro del convex Hull
5. Eliminar los puntos que queden dentro del buffer de las presencias.
6. Guardar los datos en una tabla .csv
6. Importar la tabla de datos a R.

```
Ausencias <- read.csv(file="Ausencias.csv",sep=",",header=T)
n<-length(Ausencias[,1])
n
```

### Algoritmos de modelación y selección de modelos candidatos

Es fundamental entender que los modelos de distribución de especies son herramientas numericas que combinan observaciones (presencia/ausencia o abundancia) de especies (o/y conocimiento de su ecología) con variables ambientales ([Elith et al. 2009](http://users.clas.ufl.edu/mbinford/GEOXXXX_Biogeography/LiteratureForLinks/Elith_and_Leathwick_2009_Species_distribution_models_across_space_and_time_annurev.ecolsys.110308.pdf)) y se basan en tres premisas:

1. La especie esta en equilibrio con el ambiente.
2. Los gradientes ambientales han sido estudiados apropiadamente.

__IMPORTANTE. Estas premisas son fuertes y no se mantienen para muchos modelos o generan limitaciones, sobre todo en situaciones en las cuales no hay equilibrio con el ambiente (especies invasoras, cambio climatico) y por consiguiente es más confiable basarse en _process-based models_ que en _correlative models___

La selección del algoritmo depende de varios factores, entre ellos: el conocimiento que tenemos sobre la especie, los datos de observaciones disponibles, la experticia del modelador y el tiempo disponible para hacer el modelo ([Dormann et al. 2012](https://onlinelibrary.wiley.com/doi/abs/10.1111/j.1365-2699.2011.02659.x))

{{< figure src="/images/Untitled.png" >}}

Para hacer un modelo en el cual podamos entender la información que nos da y su aplicabilidad debemos saber cuantos y cual es la calidad de los datos en terminos de esfuerzo de muestreo, identificaciones erroneas, precision en las coordenadas, etc. A medida de que cada uno de estos factores aumente se reducirá la certidumbre del modelo.

En estos casos se aconseja probar con varios algoritmos para poder evaluar con el algoritmo afecta el modelo y seleccionar el algoritmo que mejor se ajuste al modelo propuesto. También existe la posibilidad de hacer modelos ensamble, caso en el cual se usa el resultado de varios algoritmos para sacar un modelo compuesto.

### Modelando

Para los modelos usaremos el paquete de R biomod2 para lo cual debemos seguir unos pasos de preparación  de datos:

```
#presencias

OCC<-as.data.frame(bomb.occ@coords)

nP<-length(OCC[,1])
nP
myRespP<-rep(1, nP)

#Ausencias
nA<-length(Ausencias[,1])
nA
myRespA <- rep(0,nA)

myResp<-c(myRespP, myRespA)

myRespCoordA <- Ausencias[,2:3]
names(myRespCoordA)<-c("X","Y")

myRespName <- 'BradVari'
myRespCoordP <- OCC
names(myRespCoordP)<-c("X","Y")


MySppPA<-rbind(myRespCoordP,myRespCoordA)

myExpl <-stack(envData1)

```
Y para correr los modelos:

```
# 1. Formateo de datos
myBiomodData <- BIOMOD_FormatingData(resp.var = myResp,
                                     expl.var = myExpl,
                                     resp.xy = MySppPA,
                                     resp.name = myRespName)

# 2. Definiendo opciones de modelación
myBiomodOption <- BIOMOD_ModelingOptions()

# 3. Modelando

myBiomodModelOut <- BIOMOD_Modeling(
  myBiomodData,
  models = c('SRE','CTA','RF','MARS','FDA'),
  models.options = myBiomodOption,
  NbRunEval=1,
  DataSplit=80,
  Prevalence=0.5,
  VarImport=3,
  models.eval.meth = c('TSS','ROC'),
  SaveObj = TRUE,
  rescal.all.models = TRUE,
  do.full.models = FALSE,
  modeling.id = paste(myRespName,"FirstModeling",sep=""))


# Resumen de los modelos creados
myBiomodModelOut

myBiomodModelEval <- get_evaluations(myBiomodModelOut)

myBiomodModelEval["ROC","Testing.data",,,]

get_variables_importance(myBiomodModelOut)

# Creando Mapas

myBiomodProj <- BIOMOD_Projection(
  modeling.output = myBiomodModelOut,
  new.env = myExpl,
  proj.name = 'current',
  selected.models = 'all',
  binary.meth = 'TSS',
  compress = 'xz',
  clamping.mask = FALSE,
  output.format = '.grd')

# Ver mapa
plot(myBiomodProj, str.grep = 'RF')
```
Con base en los modelos obtenidos podemos generar un modelo que ensamble el resultado de los modelos obtenidos:
```
BIOMOD_EnsembleModeling( myBiomodModelOut,
                         chosen.models = 'all',
                         eval.metric = 'all',
                         eval.metric.quality.threshold = NULL,
                         models.eval.meth = c('TSS','ROC'),
                         prob.mean = TRUE,
                         prob.cv = FALSE,
                         prob.ci = FALSE,
                         prob.ci.alpha = 0.05,
                         prob.median = FALSE,
                         committee.averaging = FALSE,
                         prob.mean.weight = FALSE,
                         prob.mean.weight.decay = 'proportional',
                         VarImport = 0)
```

Wow! Que modelo! Pero recuerden __ESTO SOLO ES EL PRINCIPIO!!!__

Para aprender más sobre Biomod2 pueden seguir este tutorial: [http://www.will.chez-alice.fr/pdf/BiomodTutorial.pdf](http://www.will.chez-alice.fr/pdf/BiomodTutorial.pdf) asi como otros tutoriales que encuentran en linea.
