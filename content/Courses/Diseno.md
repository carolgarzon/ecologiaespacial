---
title: "Ejercicio - Diseño de experimento"
date: 2018-08-17T15:18:35-05:00
description: "Herramientas para selección de areas de muestreo"
featured_image: "/images/programa2.png"
draft: false
type: page
---

En este ejercicio seleccionaremos capas de Bioclim y generaremos capas raster de acumulación y flujo de agua, y prepararemos una interpolación de densidad de plantas en GRASS GIS para luego pasar a R donde usando el metodo implementado en iSDM identificaremos puntos de muestreo.

<br />
<br />

## Preprocesamiento en GRASS GIS

Revisar la resolución, si no es 00:00:30 entonces cambiarla para que corresponda con las capas raster.

```
g.region -p

g.region res=00:00:30 -p

d.mon wx0  #Abrir una ventana de display
```
### Importar capas raster - DEM y BIOCLIM

Capa DEM (Modelo de elevación digital)

```
r.in.gdal --overwrite input=/Users/carolgarzon/Downloads/srtm_22_12/srtm_22_12.tif  output=DEM_Amazonas

```

Capas de Temperatura (Bio1) y Precipitación (Bio12) de Bioclim. Antes de importar estas cpaas debemos hacer una correción porque estan por fuera de los limites geográficos, asi que debemos reparar la extensión del raster usando el comando _gdal_translate_ ,
para luego importarla en grass usando el comando _r.import_.

```
gdal_translate -a_ullr -180 90 180 -60 CHELSA_bio10_1.tif bio1_fixed.tif
r.import input= "bio1_fixed.tif" output=bioclim01 resample=bilinear extent=region resolution=region -n

gdal_translate -a_ullr -180 90 180 -60 CHELSA_bio10_12.tif bio12_fixed.tif
r.import input= "bio12_fixed.tif" output=bioclim12 resample=bilinear extent=region resolution=region -n
```


### Calcular acumulación y pendiente a partir del DEM

Primero debemos asegurarnos de que la region computacional corresponda al área del DEM y luego utilizando el comando _r.terraflow_ generar las capas de acumulacion y direccion del flujo:


```
g.region raster=DEM_Amazonas -p     #Establecer el DEM como region computacional

r.terraflow elevation=DEM_Amazonas direction=direccionAm accumulation=AcumulacionAm

```
{{< figure src="/images/Acumulacion.png" >}}

### Generar un mapa de densidad de plantas a partir de un set de puntos creados al azar


Para obtener un mapa de densidad a partir de la abundancia de plantas en estos puntos (columna: sample) se genera un mapa de densidad con base en el metodo de _Inverse distance Weighted_ que asume que el efecto de cada punto se reduce con la distancia (basicamente siguiendo la Ley de Tobler) y usando la columna de _sample_ para proveer el valor con el que se hará la interpolación:

```
r.mask DEM_Amazonas    #Establecer que la interpolación es solo para el area del DEM_Amazonas
v.surf.idw input=puntos column=sample output=DensidadP              

```
## EN R

```
install.packages("devtools", "iSDM")

library(iSDM)
library(raster)
library(mapview)

```

### Configurar GRASS en R

Preparemos nuestra sesion en R:

```
myGRASS <- "C:/Program Files/GRASS GIS 7.4.0"  #Set the location of the grass binaries and libraries
myGISDbase <- "C:/Users/cx.garzon/Documents/grassdata/"  #Set the directory path to your GISDbase
myLocation <- "GBIF"  #Name the Location
myMapset <- "cx.garzon"  #Name the mapset
```
### Iniciar la sesion de GRASS GIS

```
initGRASS(gisBase = myGRASS, home = tempdir(), gisDbase = myGISDbase, location = myLocation,
          mapset = myMapset, override = TRUE)
```
### Establecer la region


```
execGRASS("g.region", flags=c("p"), raster="DEM_Amazonas")
```
### Leer capas raster y vector

```
BIO1<-readRAST("bioclim01")                                                                                                                                      
BIO12<-readRAST("bioclim12")

Acumulacion<-readRAST("AcumulacionAm")

Direccion<- readRAST("direccionAm")

DensidadP<-readRAST("DensidadP")

Puntos<- readVECT("Puntos")


mapview(BIO1) + DensidadP + Puntos
```
Ahora se deben poner todas las capas ambientales en un solo grupo:

```
envData<-stack(raster(BIO1), raster(Acumulacion), raster(Direccion), raster(DensidadP))
```
Y a partir de estos datos, utilizando el paquete _iSDM_ se seleccionaran las areas de muestreo mas diversas, con el fin de capturar la heterogeneidad ambiental de la zona

```
library(iSDM)
MySampling <- eSample(envData, nExpect = 50, plot = TRUE, saveShape = TRUE,
    lowerLim = 0.001, upperLim = 0.999, nf = 3)
```

Y el resultado es:

```
mapview(MySampling[[1]])
```
