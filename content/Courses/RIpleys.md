---
title: "Ejercicio - Patrones de puntos"
date: 2018-08-17T15:18:35-05:00
description: "Analisis de Ripleys K y CSR"
featured_image: "/images/programa3.png"
draft: false
type: page
---

Para los análisis de patrones de puntos se han desarrollado varios tipos de análisis que permiten cuantificar y simular las caracteristicas del arreglo espacial analizado. Estos analisis tienen utilidad en ecología, astronomia, sociología y economía (entre otros).

El cálculo de la intensidad de un patron de puntos es uno de esos análisis que varia con la definición del área de estudio.

En este ejercicio trabajaremos con el shapefile de Puntos que utilizamos en la clase pasada. Aprenderemos a generar puntos al azar y exploraremos algunos analisis de puntos disponibles en GRASS.

<br />
<br />

## EN GRASS GIS

### Crear el convex hull y calcular área y número de puntos

Primero abramos la localización WGS84 y el mapset en el que hemos trabajado en los ejercicios anteriores.

Vamos a importar un archivo de puntos en formato csv usando el comando _v.in ascii_. Recuerden cambiar la ubicación del input a la ubicación donde guardaron el archivo de puntos en su computador.

```
v.in.ascii input=/Users/carolgarzon/Documents/Ecologia espacial/OCCsp.csv output=OCC separator=comma skip=1
```
<br />
Una vez importado pueden ir a la ventana de administrador de capas y hacer click en _Adicionar capa de vector_ y seleccione la capa OCC.

{{< figure src="/images/OCC.png" >}}



<br />

Con base en el vector de puntos desarrollaremos un convex hull:
<br />
```
v.hull input=OCC output=convexHull1
```
Ahora vamos a cuantificar el número de puntos y el area del convel hull. Para calcular la intensidad del patrón. Para esto primero hay que crear una tabla de datos para el mapa que acabamos de crear (convexHull1), luego le agregamos las columnas de numero de puntos y area para agregar los resultados.

{{< figure src="/images/convex1.png" >}}
<br />
```
#Crear la tabla y agregar las columnas de num_points y area
v.db.addtable convexHull1 columns="num_points integer"
v.db.addcolumn convexHull1 columns="area double precision"

#Calcular el numero de puntos en el convex Hull y alamacenarlo en la columna num_points, hacer los mismo con el area.
v.vect.stats points=OCC areas=convexHull1 count_column=num_points
v.to.db map=convexHull1 option=area columns=area  
```
<br />
<br />

1. Cuantos puntos hay? <br />
2. Cual es el área del ConvexHull1? <br />
3. En que unidad esta el área? <br />
4. Cuál es el área en Kilometros cuadrados? <br />

Calculemos la intensidad que es igual a: Número de puntos / Area <br />

5. Cual es la intensidad del patrón de puntos?<br />

<br />

### Otros análisis en GRASS GIS: identificación de clusters y creación de puntos al azar

El proceso de identificación de cluster se hace siguiendo cuatro metodos que pueden ser consultados en:

[v.cluster](https://grass.osgeo.org/grass77/manuals/v.cluster.html)

```
v.cluster --overwrite input=OCC output=clusters min=12 --o
v.colors map=clusters layer=2 use=cat color=random
```
<br />
En GRASS GIS también se pueden crear puntos al azar:

```
v.random --overwrite output=lospuntos npoints=500                 
```
<br />

{{< figure src="/images/randomCluster.png" >}}

<br />


Antes de salir exportemos el shapefile de puntos y el poligono de convexhull, hay dos formas, desde el administrador de capas, hacer click derecho sobre la capa que vamos a exportar:
<br />
{{< figure src="/images/Exportar1.png" >}}
<br />
Se abre una ventana, en esa ventana nos aseguramos que la capa en "_Nombre de capa vectorial a exportar_" sea la de Puntos, luego debemos hacer click en Browse para buscar la carpeta donde vamos a ubicar esta capa. Una vez seleccionada escribimos en el espacio _Guardar como_ el nombre de la capa, la vamos a llamar __PuntosNEW.shp__, click en guardar.
<br />
De vuelta en la ventana de grass debemos buscar en la lista de formato "_formato de datos al cual escribir_" el formato ESRI shapefile y click en correr.
<br />
{{< figure src="/images/convex4.png" >}}
<br />
O podemos exportarlos usando la linea de comando: _Recuerden cambiar el output a la ubicación de la carpeta en su computador_.
<br />
```
v.out.ogr input=OCC output=/Users/carolgarzon/PuntosNEW.shp format=ESRI_Shapefile

```
Recuerden seguir los mismos pasos para exportar el convexHull
<br />
<br />

## EN R  
<br />
Ahora utilicemos herramientas de R para hacer analisis de patrones de puntos. En R el paquete más usado para este tipo de análisis es _spatstat_. Usando este paquete importaremos un set de puntos y lo analizaremos. Hoy vamos a utilizar otra forma de importar puntos:
<br />
```
getwd()
setwd() # Agregar la ubicacion del folder que contiene los shapefile

install.packages("spatstat", "maptools", "raster", "maptools", "rgdal", "sf")
library(spatstat)
library(maptools)
library(raster)
library(rgdal)
library(sf)

```
Importamos el archivo de convex hull que será la ventana de trabajo. Pero primero exploremos las propiedades de esta capa:


```
convexhull<-readOGR("ConvexHull1.shp")
class(convexhull)

crs(convexhull)

extent(convexhull)

#Para establecer como ventana de trabajo:

xcoords<-rev(convexhull@polygons[[1]]@Polygons[[1]]@coords[,1])
ycoords<-rev(convexhull@polygons[[1]]@Polygons[[1]]@coords[,2])

sobre<-owin(poly=list(x=xcoords,y=ycoords))
```
Otra forma de importar datos a R es importar la tabla de datos en formato csv. Para esto debemos usar el siguiente codigo:
```
puntos<-read.table(file="OCCsp.csv",sep=",",header=T)
mispuntos <- ppp(puntos[,1], puntos[,2], window=sobre)
plot(mispuntos)
```
<br />
{{< figure src="/images/mispuntos.png" >}}

Una vez importados los datos y expresados en el formato que requiere _spatstat_ podemos ver su estructura (número de puntos, intensidad, entre otros) y hacer mapas de densidad:

```
summary(mispuntos)
plot(density(mispuntos))

plot(density(mispuntos, 3))

plot(density(mispuntos, 5))

```
<br />
{{< figure src="/images/densidadmispuntos.png" >}}

Y tambien podemos ver el diagrama de contours a partir del mapa de densidades de puntos:

```
contour(density(mispuntos), axes = FALSE)

contour(density(mispuntos, 3), axes = FALSE)

```
<br />
{{< figure src="/images/densidadmispuntos.png" >}}

Podemos ver que al aumentar el valor que aparece al lado de "mispuntos" se suaviza el gradiente de color. Esto es porque ese valor corresponde al numero de pixeles maximo que usa el comando para hacer el mapa de densidad o de contour. Si no ponemos un valor, por defecto el vaor será 1.

En este paquete también tenemos la opcion de generar cuadriculas y contar la densidad de puntos por cuadricula (de 10 x 10 en este caso):

```
cuadrante <- quadratcount(mispuntos, nx = 10, ny = 10)
cuadrante

plot(mispuntos)
plot(cuadrante, add = TRUE, cex = 1, col="blue")
```
<br />
<br />

### Función Ripleys K

La funcion Ripleys K parte de la ecuación:

Kest(r) = (a/(n * (n-1))) * sum[i,j] I(d[i,j] <= r) e[i,j])

- a = area de la ventana
- n = número de puntos
- i,j = son dos pares de puntos
- d[i,j] = distancia entre i y j
- e[i,j] = corrección del efecto de borde seleccionada

<br />

Calcular esta función y el intervalo de confianza (basado en un set de puntos al azar) con el paquete _spatstat_ es muy fácil:

```
par(mfrow = c(1, 1))
K <- Kest(mispuntos)
plot(K)

E <- envelope(Y = mispuntos, fun = Kest, nsim = 20) # 20 simulaciones al azar
plot(E)
E

```

Vemos como nuestros puntos estan más agregados que lo que predicen las simulaciones al azar. Al correr E podemos ver que significa cada uno de los valores del plot.

<br />

### Test de CSR

Podemos calcular, usando Chi cuadrado y la técnica de calculo de densidad por cuadrantes:


```
quadrat.test(mispuntos)
quadrat.test(mispuntos, 2,2) #Al establecer el tamaño del cuadrante 2,2 el área se divide en 4 cuadrantes.

```
Es importante tener en cuenta que este metodo de cuadrantes no tiene en cuenta la forma como esta distribuidos los puntos con respecto a otros (patrones), si no más bien como estan dispersos en el espacio. ALgunos problemas que puede tener este metodo es que si el cuadrante es muy grande contenerá pocos puntos y si es muy  pequeño tendra demasiados puntos.

<br />

### Test de Vecino más cercano (Nearest neighbor analysis - G function)

Permite calcular la frecuencia cumulativa de vecinos cercanos. Para esto vamos a dar una serie de distancias a las cuales queremos que se mida (r) para elaborar la gráfica.

```
r <- seq(0,1,by=0.005) #Como las unidades son km2 debemos usar decimales para capturar los patrones a pequeñas escalas.
r

G <- envelope(mispuntos, Gest, r=r, nsim = 59, rank = 2)
plot(G)
G

```
<br />
Para ver la descripción de la gráfica de función G, debemos correr G.

{{< figure src="/images/Gfun.png" >}}

### Crear puntos al azar

Para crear puntos al azar usaremos el paquete _sf_, este es un paquete con muchisimas funciones que les recomiendo explorar (al igual que el paquete _spatstat_):

```
azar <- spsample(convexhull,n=200,"random")
azar
plot(convexhull)
plot(azar, add=T)

```
<br />
{{< figure src="/images/Azar.png" >}}
<br />
Que tal calcular funciones K y G en estos datos? que esperamos obtener?

- En que tipo de preguntas podemos aplicar este tipo de funciones y simulaciones?

## Otras herramientas para análisis de puntos

### Kernel density estimation

Para esta seccion usaremos los siguientes paquetes:
```
install.packages("GISTools", "fMultivar", "ggplot2")
library(GISTools, fMultivar, ggplot2)
```

KDE significa kernel density estimation. El KDE determinado set de puntos (mispuntos1) es calculado estimando la distancia entre puntos. Menores distancias implican mayor densidad. El KDE utiliza la variable ancho de banda (bandwidth, h) para establecer que tan suaves son las curvas de densidad.

```
mispuntos1<-SpatialPoints(puntos[,1:2], proj4string = CRS("+proj=longlat +datum=WGS84 +no_defs +ellps=WGS84 +towgs84=0,0,0"))
densidad<-kde.points(mispuntos1, h= 2, lims=convexhull)
level.plot(densidad)
mascara<- poly.outer(densidad, convexhull)
add.masking(mascara)
plot(convexhull, add=T)
```
<br />
{{< figure src="/images/KDEplot.png" >}}
<br />
Cambia los valores de h (3, 5 o 10) y mira como cambian las curvas del plot de densidad.

### Hexagonal binning

Otra forma de visualizar los cambios en la densidad de puntos es usando el "_hexagonal binning_". Para esta funcion se usa una grilla de celdas hexagonales que se sobrepone a la capa de puntos y se cuenta cuantos puntos hay en cada celda. El comando que permite hacer esta operación es __hexBinning__ del paquete fMultivar en combinación con el paquete ggplot2. _Recuerden que a las graficas de ggplot2 les pueden hacer toda clase de ediciones y cambios._

```
hbins <- hexBinning(coordinates(mispuntos1))
hbins<-fortify(cbind.data.frame(hbins$x,hbins$y,hbins$z))

ggplot(hbins, aes(hbins[,1], hbins[,2])) +
  stat_summary_hex(aes(z = hbins[,3])) +
  scale_fill_continuous(low = 'blue', high = 'red')
```
<br />
{{< figure src="/images/Hexagonbin.png" >}}
<br />
- Calculemos la autocorrelacion y el Ripleys K del set de puntos que hemos trabajado en este taller, con base en ese calculo podemos seleccionar el bandwidth del KDE?



__Un mundo de puntos por explorar!!!__
