---
title: "Instalando GRASS GIS"
date: 2018-09-17T13:40:08-05:00
description: "Paso a paso para instalar GRASS GIS en WIndows, Mac y Linux"
featured_image: "/images/programa1.png"
draft: false
type: page
---

En esta clase usaremos el programa GRASS GIS 7.4.1 (version estable). Este programa puede ser instalado en diferentes sistemas operativos (Windows, Mac y Linux).

<br />


### Windows

Antes de instalar GRASS es importante saber si el computador es 32 o 64 bit. Para averiguarlo deben hacer click derecho sobre el icono de su equipo y en el menu que se ve seleccionar Propiedades. Esto abrirá una ventana en la que podran ver la version.

<br />
<br />
{{< figure src="/images/32o64.jpg" >}}
<br />
<br />
Para instalar GRASS GIS:

1.  Bajar el instalador de GRASS GIS en la version de [[32-bit](https://grass.osgeo.org/grass74/binary/mswindows/native/x86/WinGRASS-7.4.0-1-Setup-x86.exe) o [64-bit](https://grass.osgeo.org/grass74/binary/mswindows/native/x86_64/WinGRASS-7.4.0-1-Setup-x86_64.exe)

2.  Corra el instalador (debe hacerlo en un computador con acceso a internet)

<br />
<br />

### MAC OS

Se puede instalar GRASS GIS 7.4 usando la version en el siguiente link:

[http://grassmac.wikidot.com/downloads](http://grassmac.wikidot.com/downloads)

Baje la version 7.4.1. Se bajará el archivo grass-7.4.1.dmg, abralo y arrastre el icono de GRASS GIS a Aplicaciones. Para abrir GRASS GIS debe hacer click derecho sobre el icono y seleccionar abrir, esto evitará que el sistema bloquee el programa por ser de codigo abierto.

<br />
<br />

### Ubuntu Linux

Puede instalar GRASS GIS 7.4 desde los paquetes:

```
    sudo add-apt-repository ppa:ubuntugis/ubuntugis-unstable
    sudo apt-get update
    sudo apt-get install grass
```
<br />
<br />

Bienvenido a GRASS GIS
----------------------

Una vez instalado abra el programa. Aparecerá una ventana como esta:
<br />
<br />
{{< figure src="/images/PrimeraGRASS.png" >}}

<br />
<br />

### Estructura de GRASS GIS

Teniendo en cuenta la estructura de GRASS:  
<br />
<br />
{{< figure src="/images/estructura.png" >}}
<br />
<br />


### Ubicación de la base de datos

Es importante identificar la ubicación de la base de datos (carpeta de
grassdata), la localización (el sistema de coordenadas en los que
trabajaremos) y el mapset (el usuario de los mapas).

Lo primero que debemos hacer es crear una carpeta que se llame
*grassdata*. En esa carpeta guardaremos todo lo que se haga en GRASS. Despues de creada, en la casilla de seleccion del directorio de
trabajo debemos proveer la ubicación de la carpeta *grassdata* que
acabamos de crear.
<br />
<br />
{{< figure src="/images/grassdata.png" >}}


<br />
<br />

### Creación de la locación

Ahora debemos crear una locación, esta determinará el sistema de
coordenadas que usaremos.

1.  Hacer click en Nueva locación  

2.  En la ventana que se abre, asignar un nombre de Locación. **Es
    aconsejable que se relacione/describa la proyeccion que se va
    usar**  
<br />
<br />
{{< figure src="/images/Location1.png" >}}
<br />
<br />


1.  Click en Siguiente.  

2.  Aparecerá una lista de opciones para asignar la proyeccion que se
    va usar. Cada selección ofrece una estrategia diferente para elegir
    la proyeccion:  

<br />
<br />
{{< figure src="/images/Location2.png" >}}
<br />
<br />
<!-- -->

1.  **Codigo EPSG-.** Aparecerá una ventana en la que se puede buscar el
    codigo o se puede asignar un codigo directamente. Pueden consultar
    sobre estos codigos en la pagina [http://spatialreference.org/ref/epsg/](http://spatialreference.org/ref/epsg/)

2.  **Leer la proyeccion de un archivo georeferenciado** En este caso se
    debe tener un archivo que ya tenga asignado un sistema
    de georeferencia. La opción permitirá escoger este archivo.  
3.  **Leer proyeccion de un archivo .proj** Esta opción permite usar un
    archivo de tipo .proj para asignar la proyección.  
4.  **Crear un sistema cartesiano generico (X,Y)** El programa
    automaticamente generará una proyección aritificial basada en un
    plano cartesiano.



5. Despues de seleccionar el tipo de proyeccion se hace click en
Siguiente, y se selecciona *1. Usar toda la region* y Aceptar.  

6. Saldrá un resumen de la locación creada y se puede hacer click en
Finalizar.  


En la casilla de Locación aparecerá la locación creada.
<br />
<br />

### Creación del mapset

En la última parte de esta fase de configuración de la base de datos
requiere la generación de un mapset (usuario) de los datos. Esta puede
ser establecida a partir de una persona que usa los datos o de un
proyecto para el cual se van a necesitar los datos en esta Locación.

Al hacer click en Crear mapset se verá una ventana con una opción de
nombre de mapset asignada. Podemos cambiar o dejar esta opción.  

<br />
<br />
{{< figure src="/images/Location3.png" >}}
<br />
<br />

Ahora, el boton de iniciar GRASS GIS estará resaltado y haciendo click
en el podremos iniciar la sesion de GRASS.
<br />
<br />

Ambiente GRASS
--------------


Ahora se abrirán las tres ventanas básicas de GRASS.  

- **Administrador de capas** Esta ventana contiene la mayor parte de las
funciones de Grass en el formato de GUI. Al desplegar la barra de
herramientas se despliegan opciones de Configuración, analisis y
procesamiento de vectores y rasters y otras funciones de procesamiento y
analisis de images, bases de datos y archivos 3D. El administrados de
capas cuenta con 4 pestañas: En la pestaña **capas** se pueden ver las
capas de raster y vector que se van adicionando. En la pestaña
**consola** se pueden agregar y correr comandos. En la pestaña de
**modulos** se tiene acceso a la lista de modulos y funciones de GRASS
GIS. En la pestaña **Data** se puede ver el arbol de datos disponibles
en GRASS. Y finalmente, en la pestaña de **python** se pueden generar
funciones en lenguaje python.  

- **Map display** En esta ventana aparecerán las capas que se visualicen
(comandos *d.vect* o *d.rast*). Hay opciones para agregar leyenda,
escala, texto y flecha de norte a los mapas. Se encuentran el
Digitalizador de vectores y rasters. Y herramientas de analisis como
medición de distancias y areas.  

 - **Terminal** Esta ventana es muy muy importante! Desde aqui se pueden
escribir los codigos para correr en GRASS y se tiene toda la
funcionalidad de GRASS disponible. También, en caso de que se cierre el
GUI (Map display y Administrdor de capas) se puede volver a abrir desde
aqui escribiendo el comando *g.gui wxpython*  


<br />
<br />
{{< figure src="/images/ambiente.png" >}}

<br />
<br />


### Extensiones en GRASS GIS (Addons)

Como en R, varias de las funciones estan agrupadas en modulos que han
sido creados por la comunidad de desarrolladores y usuarios de GRASS
GIS, a estos modulos se les conoce como Extensiones o Addons. En este
link pueden consultar una lista de las extensiones disponibles en GRASS
GIS:
[https://grasswiki.osgeo.org/wiki/AddOns/GRASS_7](https://grasswiki.osgeo.org/wiki/AddOns/GRASS_7)

<br />
<br />

\#\#\#\# Instalación de las extensiones

Para instalar extensiones debemos ir a la barra de herramientas y hacer
click en Install extension from addon: **Settings &gt; Addons extensions
&gt; Install extension from addons**

<br />
<br />
{{< figure src="/images/Addons.png" >}}
<br />
<br />
Se abrirá una ventana en la que se puede seleccionar la extension que se
va instalar. Para isntalar, por ejemplo, la extension *v.in.gbif* que es
una extensión que importa un vector de puntos a partir de los datos
bajados de GBIF, debemos expandir la opcion vector y buscar *v.in.gbif*,
seleccionarlo y hcer click en instalar.  


{{< figure src="/images/addons1.png" >}}
<br />
<br />

### La extension *v.in.gbif*

Esta extension facilita importar archivos .csv bajados directamente de
la pagina web [GBIF](https://www.gbif.org/es/). Para importarlos, desde la pestaña Consola del Administrador de
capas se debe escribir *v.in.gbif*. Se abrirá la ventana de esta
extension en la cual debemos buscar el archivo .csv de GBIF y asignar un
nombre a la capa que se importará.  

<br />
<br />
{{< figure src="/images/addons2.png" >}}
<br />
<br />
Una vez importados los datos los podemos hacer visibles en el **Map
display** agregando la capa a ventana de administrador de capas. Podemos
hacerlo de tres formas:  

1. Desde la ventana de administrador de capas usando el boton "Add
vector map layer", desplegando la lista de vectores y adicionando el
vector de datos que se acaba de importar.  
2. Desde la consola escribiendo *d.vect \[nombre del vector GBIF
importado\]*  
3. Desde el terminal, abriendo una ventana y luego visualizando el
vector en esa ventana:

```
    d.mon wx0
    d.vect datosGBIF
```
<br />
<br />
### Preparando los datos para las clases

En las clases usaremos un set de datos que han preparado los creadores de GRASS GIS para entrenamientos en GRASS GIS. Para esto debemos salir de GRASS GIS y volver a entrar. Una vez en la ventana de inicio de GRASS debemos hacer click en Download, ubicado debajo de New en Locación. Se deplegará una ventana con varios sets de datos para descargar. Seleccionen los datos de __Complete NC Location__ y click en Download. Una vez los datos acaben de bajar se creara una nueva locacion llamada __nc_spm_08_grass7__.

<br />
<br />
{{< figure src="/images/downloadData.png" >}}
<br />
<br />
